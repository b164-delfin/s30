const express = require("express");

const mongoose =require("mongoose");


const app = express();

const port = 4000;

mongoose.connect('mongodb+srv://kyoko:Kyoko1990@cluster0.hxds7.mongodb.net/batch164_to-do?retryWrites=true&w=majority', 
{
	useNewUrlParser: true,
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))




app.use(express.json());

app.use(express.urlencoded({ extended:true }));


const userSchema = new mongoose.Schema({
	username : {
		type: String,
		required : true
	},
	password : {
		type : String,
		required : true
	}
})



const Users = new mongoose.model("USER", userSchema);

module.exports = Users;


app.post("/signup", async (req, res) =>{
	try {
		const username = req.body.username;
		const password = req.body.password;

		const createUser = new Users({
			username : username,
			password : password
		});

		const created = await createUser.save();
		console.log(created);
		res.status(200).send("Registered");
	} catch (error) {
		res.status(400).send("Already Registered");
	}
})


app.get('/users', (req, res) => {
	res.send(Users)
});




app.listen(port, () => console.log(`Server running at port ${port}`));

